package com.test.authentication;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.config.ConfigFront;
import com.config.UserItem;
import com.joomla.home.CreateAccountPage;
import com.joomla.home.HomePage;

/**
 * <h2>Joomla - Testing Create Account (Negative)</h2>
 * <p> Duplicate Users - by name, email or username
 * <p> Wrong Confirmation in Form - password or email
 * 
 * @author Tase Gula
 */
public class TestCreateAccountNegative
{
	private static WebDriver driver;
	private static HomePage joomlaHome;
	private static CreateAccountPage joomlaCreateAccount;
	
	
	// ************************************************************************
	// TESTS
	// ************************************************************************
	
	@Test (description   = "Register user with the same NAME")
	public void createDuplicateName() {
		driver.get(ConfigFront.PageUrl.createAccount);
		joomlaCreateAccount.createFor(UserItem.DUMMY);
		
		// the link changes to confirm completion
		assertNotEquals(driver.getCurrentUrl(), ConfigFront.PageUrl.createAccount);
		
		// try to login
		driver.get(ConfigFront.PageUrl.home);
		joomlaHome.login(UserItem.DUMMY);
		assertEquals(driver.findElement(ConfigFront.alertBody).getText(), ConfigFront.AlertMessage.loginError);
	}
	
	@Test (description   = "Try to register user with the same USERNAME")
	public void createDuplicateUsername() {
		driver.get(ConfigFront.PageUrl.createAccount);
		joomlaCreateAccount.createFor(UserItem.DUP_USER);
		
		// Error message
		assertEquals(driver.findElement(ConfigFront.alertBody).getText(), ConfigFront.AlertMessage.createAccountUsernameInUse);
	}
	
	@Test (description   = "Try to register user with the same MAIL")
	public void createDuplicateMail() {
		driver.get(ConfigFront.PageUrl.createAccount);
		joomlaCreateAccount.createFor(UserItem.DUP_MAIL);
		
		// Error message
		assertEquals(driver.findElement(ConfigFront.alertBody).getText(), ConfigFront.AlertMessage.createAccountMailInUse);
	}
	
	@Test (description   = "User enters a valid username and email, but the confirmation for password is wrong")
	public void wrongConfirmationPassword() {
		joomlaCreateAccount.createFor(UserItem.COMF_PASS);
		
		// Page should not change; If it's ok, the link changes to confirm completion
		assertEquals(driver.getCurrentUrl(), ConfigFront.PageUrl.createAccount);
	}
	
	@Test (description   = "User enters a valid username and email, but the confirmation for email is wrong")
	public void wrongConfirmationMail() {
		joomlaCreateAccount.createFor(UserItem.COMF_MAIL);
		
		// Page should not change; If it's ok, the link changes to confirm completion
		assertEquals(driver.getCurrentUrl(), ConfigFront.PageUrl.createAccount);
	}
	
	
	// ************************************************************************
	// CONFIGURATION
	// ************************************************************************
	
	@AfterMethod
	public void afterMethod() {
		driver.manage().deleteAllCookies();
		driver.get(ConfigFront.PageUrl.createAccount);
	}
	
	@BeforeClass
	public void before() {
		// Create a new instance of a driver and navigate to the right place :D
		driver = new FirefoxDriver();
		driver.get(ConfigFront.PageUrl.createAccount);
		
		// Just some settings
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// Create a new instance of Joomla Home Page
		joomlaHome = PageFactory.initElements(driver, HomePage.class);
		joomlaCreateAccount = joomlaHome.gotoCreateAccount();
	}
	
	@AfterClass
	public void after() {
		driver.close();
		driver.quit();
	}
}
