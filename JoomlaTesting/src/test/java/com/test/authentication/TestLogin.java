package com.test.authentication;

import static org.testng.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.config.ConfigAdmin;
import com.config.ConfigFront;
import com.config.User;
import com.config.UserItem;
import com.joomla.admin.AdminLoginPage;
import com.joomla.home.HomePage;

/**
 * <h2>Testing Joomla</h2>
 * <p> Login under different scenarios
 * <p> Logout under those scenarios
 * 
 * @author Tase Gula
 */
public class TestLogin
{
	private static WebDriver driver;
	private static HomePage joomlaHome;
	private static AdminLoginPage adminHome;
	
	
	// ************************************************************************
	// DATA PROVIDERS
	// ************************************************************************
	
	@DataProvider(name = "testUsersLogin")
	public Object[][] usersLogin() {
		return new Object[][] {
				{ UserItem.TASE,     true },
				{ UserItem.DUMMY,    false },
				{ UserItem.DUP_USER, false }, // has the same username as DAVID
			};
	}
	
	@DataProvider(name = "testAdminsLogin")
	public Object[][] adminsLogin() {
		return new Object[][] {
				{ ConfigAdmin.ROOT, true },
				{ UserItem.DAVID,   false },
			};
	}
	
	// ************************************************************************
	// TESTS
	// ************************************************************************
	
	@Test (description       = "Try to login in Joomla Home Page with the given Users",
			dataProvider     = "testUsersLogin")
	public void loginUsers(User user, boolean success) {
		driver.get(ConfigFront.PageUrl.home);
		joomlaHome.login(user);
		
		// a message appears when login fails
		boolean actual = driver.findElements(ConfigFront.alertBody).size() == 0;
		assertEquals(actual, success);
		
		if (success) {
			joomlaHome.logout();
			assertEquals(driver.getCurrentUrl(), ConfigFront.PageUrl.home);
		}
	}
	
	
	@Test (description       = "Try to login in Joomla Administration Page with the given Users",
			dataProvider     = "testAdminsLogin")
	public void loginAdmins(User user, boolean success) throws InterruptedException {
		driver.get(ConfigAdmin.PageUrl.adminLogin);
		adminHome.login(user);
		
		// warning is displayed only at failed login
		boolean actual = driver.findElements(ConfigAdmin.alertBody).size() == 0;
		assertEquals(actual, success);
		
		if (success) {
			adminHome.logout();
			assertEquals(driver.getCurrentUrl(), ConfigAdmin.PageUrl.adminHome);
		}
	}
	
	
	// ************************************************************************
	// CONFIGURATION
	// ************************************************************************
	
	@AfterMethod (description = "Delete cookies")
	public void afterUsersTest() {
		driver.manage().deleteAllCookies();
	}
	
	@BeforeClass
	public void before() {
		// Create a new instance of a driver
		driver = new FirefoxDriver();
		
		// Just some settings
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// Create a new instance of Joomla Home Page
		joomlaHome = PageFactory.initElements(driver, HomePage.class);
		adminHome = new AdminLoginPage(driver);
	}
	
	@AfterClass
	public void after() {
		driver.close();
		driver.quit();
	}
}
