package com.test.authentication;

import static org.testng.Assert.*;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.config.ConfigFront;
import com.config.User;
import com.config.UserItem;
import com.google.Gmail;
import com.google.GoogleLogin;
import com.joomla.home.RecoverUsernamePage;
import com.joomla.home.HomePage;

/**
 * <h2>Joomla - Testing Recover Username</h2>
 * <p> Recover username for a valid Email address (registered user)
 * 
 * @author Tase Gula
 */
public class TestRecoverUsername
{
	private static WebDriver driver;
	private static WebDriverWait waitDriver;
	
	private static HomePage joomlaHome;
	private static RecoverUsernamePage joomlaRecoverUser;

	private static By messageLoc     = By.id("system-message");	
	private static String message    = "Reminder successfully sent. Please check your mail."; 
	
	private static String emailTitle = "Your Testing Stuff username";
	
	
	// ************************************************************************
	// DATA PROVIDERS
	// ************************************************************************
	
	@DataProvider (name = "testRecoverUsername")
	public Object[][] usersRecoverUsername() {
		return new Object[][] {
				{ UserItem.DAVID },
		};
	}
	
	
	// ************************************************************************
	// TESTS
	// ************************************************************************
	
	@Test (dataProvider = "testRecoverUsername")
	public void requestUsername(User user) {
		// request recovering username
		joomlaRecoverUser.recoverFor(user.mailUser());
		assertTrue(driver.findElement(messageLoc).getText().contains(message));
	}
	
	
	@Test (dataProvider      = "testRecoverUsername", 
			dependsOnMethods = { "requestUsername" })
	public void recoverUsername(User user) {
		// Goto Gmail
		GoogleLogin google = new GoogleLogin(driver);
		google.login(user.mailUser(), user.mailPass());
		Gmail gmail = google.gotoGmail();
		gmail.goToMail(emailTitle);

		// Test if the email send the real username
		By emailBodyLoc = By.xpath("//div[@class='a3s' and contains(.,'username')]");
		waitDriver.until(ExpectedConditions.presenceOfElementLocated(emailBodyLoc));
		String emailText = driver.findElement(emailBodyLoc).getText();
		
		assertTrue(emailText.contains("Your username is " + user.username()));
	}
	
	
	// ************************************************************************
	// CONFIGURATION
	// ************************************************************************
	
	@BeforeClass
	public void before() {
		// Create a new instance of a driver
		driver     = new FirefoxDriver();
		waitDriver = new WebDriverWait(driver, 10);
		
		// Navigate to the right place
		driver.get(ConfigFront.PageUrl.home);
		
		// Just some settings
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// Create a new instance of Joomla Home Page
		joomlaHome = PageFactory.initElements(driver, HomePage.class);
		joomlaRecoverUser = joomlaHome.gotoForgotUsername();
	}
	
	@AfterClass
	public void after() {
		driver.close();
		driver.quit();
	}
}
