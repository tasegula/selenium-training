package com.test.authentication;

import static org.testng.Assert.*;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.config.ConfigFront;
import com.config.User;
import com.config.UserItem;
import com.google.Gmail;
import com.google.GoogleLogin;
import com.joomla.home.RecoverPasswordPage;
import com.joomla.home.HomePage;
import com.joomla.home.LoginPage;

/**
 * <h2>Joomla - Testing Recover Password</h2>
 * <p> Recover password for a valid Email address (registered user)
 * 
 * @author Tase Gula
 */
public class TestRecoverPassword
{
	private static WebDriver driver;
	
	private static HomePage joomlaHome;
	private static RecoverPasswordPage joomlaRecoverPass;
	private static LoginPage joomlaLogin;

	private static By messageLoc     = By.className("form-validate");
	private static String message    = "An email has been sent to your email address.";
	
	private static String emailTitle = "Your Testing Stuff password reset request";
	
	// ************************************************************************
	// DATA PROVIDERS
	// ************************************************************************
	
	@DataProvider (name = "testRecoverPassword")
	public Object[][] usersRecoverPassword() {
		return new Object[][] {
				{ UserItem.DAVID},
		};
	}
	
	
	// ************************************************************************
	// TESTS
	// ************************************************************************
	
	@Test (description   = "Request password recovering",
			dataProvider = "testRecoverPassword")
	public void requestPassword(User user) {
		// request recovering password
		joomlaRecoverPass.recoverFor(user.mailUser());
		assertTrue(driver.findElement(messageLoc).getText().contains(message));
	}
	
	@Test (description       = "Go to Gmail and click the link",
			dataProvider     = "testRecoverPassword",
			dependsOnMethods = "requestPassword")
	public void testLink(User user) {
		// Go to Gmail
		GoogleLogin google = new GoogleLogin(driver);
		google.login(user.mailUser(), user.mailPass());
		Gmail gmail = google.gotoGmail();
		gmail.goToMail(emailTitle);
		
		// Click Activation Link -> opens new Window (close gmail)
		driver.findElement(By.xpath("//a[contains(@href, '/joomla/')]")).click();
		
		// Switch to new window opened
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		
		joomlaRecoverPass.validateUsername(user.username());
		assertEquals(driver.getCurrentUrl(), ConfigFront.PageUrl.recoverPassword);
	}
	
	@Test (description       = "Add a new password",
			dataProvider     = "testRecoverPassword",
			dependsOnMethods = "testLink")
	public void changePassword(User user) {
		// use the alternative password (password is changed) -> page is changed (verify)
		driver.get(ConfigFront.PageUrl.recoverPassword);
		joomlaRecoverPass.typeNewPassword(UserItem.alternativePass);		
		assertEquals(driver.getCurrentUrl(), ConfigFront.PageUrl.login);
	}
	
	@Test (description       = "Try to login with old credentials",
			dataProvider     = "testRecoverPassword",
			dependsOnMethods = "changePassword")
	public void testLoginOld(User user) {
		// Try login with old password -> page doesn't change
		driver.get(ConfigFront.PageUrl.login);
		joomlaLogin.loginWith(user.username(), user.password());
		
		// Remain on login page because login failed
		assertEquals(driver.getCurrentUrl(), ConfigFront.PageUrl.login);
	}
	
	@Test (description       = "Try to login with new credentials",
			dataProvider     = "testRecoverPassword",
			dependsOnMethods = "changePassword")
	public void testLoginNew(User user) {
		// Try login with new password -> go to Profile page
		driver.get(ConfigFront.PageUrl.login);
		joomlaLogin.loginWith(user.username(), UserItem.alternativePass);
		
		// Hi, user.name()
		assertTrue(driver.findElement(By.xpath("//form[@id='login-form']/div[1]")).getText().contains(user.name()));
	}
	
	
	// ************************************************************************
	// CONFIGURATION
	// ************************************************************************
	
	@AfterMethod
	public void afterMethod() {
		driver.get(ConfigFront.PageUrl.forgotPassword);
	}
	
	@BeforeClass
	public void before() {
		// Create a new instance of a driver
		driver     = new FirefoxDriver();
		
		// Navigate to the right place
		driver.get(ConfigFront.PageUrl.home);
		
		// Just some settings
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// Create new instances of Joomla Pages
		joomlaLogin = new LoginPage(driver);
		joomlaHome  = PageFactory.initElements(driver, HomePage.class);
		joomlaRecoverPass = joomlaHome.gotoForgotPassword();
	}
	
	@AfterClass
	public void after() {
		driver.close();
		driver.quit();
	}
}
