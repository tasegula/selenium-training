package com.test.authentication;

import static org.testng.Assert.*;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.config.ConfigAdmin;
import com.config.ConfigFront;
import com.config.User;
import com.config.UserItem;
import com.google.Gmail;
import com.google.GoogleLogin;
import com.joomla.home.CreateAccountPage;
import com.joomla.home.LoginPage;

/**
 * <h2>Joomla - Testing Create Account (Positive)</h2>
 * <p> Register and activate two users; Activation is made via Gmail
 * 
 * @author Tase Gula
 */
public class TestCreateAccount
{
	// Using one WebDriver for each user;
	// The point of this App is not to test Gmail, so it doesn't switch accounts in Google
	private static WebDriver[] drivers         = new WebDriver[2];
	private static WebDriverWait[] waitDrivers = new WebDriverWait[2];
	
	private static CreateAccountPage joomlaCreateAccount[] = new CreateAccountPage[2];
	
	private static final By activationLink = By.xpath("//*[@class='adn ads']/div[2]/div[6]/div[1]/a");
	
	
	// ************************************************************************
	// DATA PROVIDERS
	// ************************************************************************
	@DataProvider(name = "testCreateAccounts")
	public Object[][] usersCreateAccount() {
		return new Object[][] {
				{ UserItem.TASE,  1 },
				{ UserItem.DAVID, 0 },          
			};
	}
	
	
	// ************************************************************************
	// TESTS
	// ************************************************************************
	
	@Test (description   = "Send requests for two new accounts",
			dataProvider = "testCreateAccounts")
	public void createAccount(User user, int id) {
		joomlaCreateAccount[id].createFor(user);
		assertEquals(drivers[id].getCurrentUrl(), "http://localhost/joomla/index.php/component/users/?view=registration&layout=complete");
	}
	
	@Test (description       = "Go to Gmail to activate account; test if the email was send",
			dataProvider     = "testCreateAccounts",
			dependsOnMethods = { "createAccount" })
	public void gotoGmail(User user, int id) {
		GoogleLogin google = new GoogleLogin(drivers[id]);
		google.login(user.mailUser(), user.mailPass());
		Gmail gmail = google.gotoGmail();
		assertTrue(gmail.goToMail("Account Details for " + user.name() + " at " + ConfigAdmin.siteName));
	}
	
	@Test (description       = "While in gmail, click on the activation link and activate the account",
			dataProvider     = "testCreateAccounts",
			dependsOnMethods = { "gotoGmail" })
	public void activateAccount(User user, int id) {
		// Click Activation Link -> opens new Window (close gmail)
		drivers[id].findElement(activationLink).click();
		drivers[id].close();

		// Switch to new window opened
		for(String winHandle : drivers[id].getWindowHandles()){
		    drivers[id].switchTo().window(winHandle);
		}

		// Verify is the correct page 
		waitDrivers[id].until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className("site-title")));
		assertEquals(drivers[id].getCurrentUrl(), ConfigFront.PageUrl.createdAccount);
	}
	
	@Test (description       = "Login with the new account",
			dataProvider     = "testCreateAccounts",
			dependsOnMethods = { "activateAccount" })
	public void loginAccount(User user, int id) {
		LoginPage loginPage = new LoginPage(drivers[id]);
		loginPage.loginWith(user.username(), user.password());
		
		// Hi, user.name()
		assertTrue(drivers[id].findElement(By.xpath("//form[@id='login-form']/div[1]")).getText().contains(user.name()));
	}
	
	
	// ************************************************************************
	// CONFIGURATION
	// ************************************************************************
	
	@BeforeClass
	public void before() {
		for (int i = 0; i < usersCreateAccount().length; i++) {
			// Create a new instance of a driver
			drivers[i]     = new FirefoxDriver();
			waitDrivers[i] = new WebDriverWait(drivers[i], 10);
			
			// Navigate to the right place
			drivers[i].get(ConfigFront.PageUrl.createAccount);
			
			// Just some settings
			drivers[i].manage().window().maximize();
			drivers[i].manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			// Create a new instance of Joomla Home Page
			joomlaCreateAccount[i] = PageFactory.initElements(drivers[i], CreateAccountPage.class);			
		}
	}
	
	@AfterClass
	public void after() {
		for (int i = 0; i < usersCreateAccount().length; i++) {
			drivers[i].close();
			drivers[i].quit();
		}
	}
}
