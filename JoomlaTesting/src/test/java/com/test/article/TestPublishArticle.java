package com.test.article;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.config.ConfigAdmin;
import com.config.ConfigFront;
import com.config.UserItem;
import com.joomla.admin.AdminLoginPage;
import com.joomla.admin.ControlPanelPage;
import com.joomla.admin.articles.ArticleManagerPage;
import com.joomla.home.HomePage;
import com.joomla.home.ArticleFormPage;

/**
 * <h2>Joomla - Testing Publishing Article</h2>
 * <p> Login as a user with rights to publish
 * <p> Publish an article
 * 
 * @author Tase Gula
 */
public class TestPublishArticle
{
	private static WebDriver driver;
	
	private static HomePage joomlaHome;
	private static ArticleFormPage joomlaSubmitArticle;
	
	private static final int n = 41; // go only up on local!
	private static final String titleAuto  = "Ana are " + n + " mere";
	private static final String titleGiven = "Ana are " + n + " mere verzi";
	private static final String aliasA = "ana-mere-" + n;
	private static final String aliasB = "ana-mere-" + n + "-1";
	
	private static final String htmlContent = "<p><h1>BREAKING NEWS</h1>\n"
											+ "<p><h2>ANA HAS <u>MERE</u></h2>\n"
											+ "<p>\n";
	private static final String frameContent = "There are rumors that Ana has finally found the mere she was looking for";
	
	
	// ************************************************************************
	// TESTS
	// ************************************************************************
	
	@Test (description = "Login a user with publishing rights - tase")
	public void loginTase() {
		driver.get(ConfigFront.PageUrl.home);
		joomlaHome.login(UserItem.TASE);
		
		// Hi, user.name();
		assertTrue(driver.findElement(By.xpath("//form[@id='login-form']/div[1]")).getText().contains(UserItem.TASE.name()));
	}
	
	@Test (description = "Login administrator")
	public void loginRoot() {
		driver.get(ConfigAdmin.PageUrl.adminLogin);
		(new AdminLoginPage(driver)).login(ConfigAdmin.ROOT);
		
		assertTrue(driver.findElement(ControlPanelPage.contentArticleMg).isDisplayed());
	}
	
	
	@Test (description       = "Go to the 'submit an article' page",
			dependsOnMethods = "loginTase")
	public void gotoSubmitArticle() {
		driver.get(ConfigFront.PageUrl.home);
		joomlaSubmitArticle = joomlaHome.gotoSubmitArticle();
		
		// Verify the url
		assertEquals(driver.getCurrentUrl(), ConfigFront.PageUrl.submitArticle);
	}
	
	
	@Test (description       = "Add a new article using both HTML and TextRich Editors, with auto-alias",
			dependsOnMethods = "gotoSubmitArticle")
	public void addArticle_AutoAlias() {
		addArticle(titleAuto, htmlContent, frameContent);
		assertEquals(driver.findElement(ConfigFront.alertBody).getText(), ConfigFront.AlertMessage.submitedArticle);
	}
	
	
	@Test (description       = "Delete an article added with auto-alias",
			dependsOnMethods = { "loginRoot" })//, "addArticle_AutoAlias" })
	public void deleteArticle_AutoAlias() {
		driver.get(ConfigAdmin.PageUrl.adminHome);		
		
		ArticleManagerPage articleManager = (new ControlPanelPage(driver)).gotoArticleManagerPage();
		articleManager.trashArticleByTitle(titleAuto);
		
		assertTrue(driver.findElement(ConfigAdmin.alertBody).getText().contains(ConfigAdmin.AlertMessage.trashedArticle));
		
		// Go back to Submit Article Page
		driver.get(ConfigFront.PageUrl.submitArticle);
	}
	
	
	@Test (description       = "Add an article with the same name as a deleted one, with auto-alias"
							 + "\n" + "This failes -> BUG / normal DB thing",
			dependsOnMethods = { "gotoSubmitArticle" })//, "deleteArticle_AutoAlias" })
	public void addDeletedArticle_AutoAlias() {
		addArticle(titleAuto, 
				   htmlContent, 
				   "Update: some technical problems deleted this message. Now is up again\n" + frameContent);
		
		assertEquals(driver.findElement(ConfigFront.alertBody).getText(), ConfigFront.AlertMessage.errorSubmitArticle);
		joomlaSubmitArticle.clear();
	}
	
	
	@Test (description       = "Add a new article with a given alias",
			dependsOnMethods = { "loginRoot", "gotoSubmitArticle" })
	public void addArticle_GivenAlias() {
		driver.get(ConfigFront.PageUrl.submitArticle);
		
		addArticleWithAlias(titleGiven, aliasA);
		assertEquals(driver.findElement(ConfigFront.alertBody).getText(), ConfigFront.AlertMessage.submitedArticle);
		
		// ok, it's added, but is it added with the correct alias in the DB?
		driver.get(ConfigAdmin.PageUrl.adminLogin);		
		ArticleManagerPage articleManager = (new ControlPanelPage(driver)).gotoArticleManagerPage();
		
		String aliasRecv = articleManager.getAlias(titleGiven);
		assertEquals(aliasRecv, aliasA);
	}
	
	@Test (description       = "Add a new article with the same name as one already added but with a different alias"
							 + "\n" + "This works -> BUG / Title can be duplicated",
			dependsOnMethods = {"gotoSubmitArticle" })//, addArticle_GivenAlias" })
	public void addArticleSameName_GivenAlias() {
		driver.get(ConfigFront.PageUrl.submitArticle);
		addArticleWithAlias(titleGiven, aliasB);
		
		// is it added?
		assertEquals(driver.findElement(ConfigFront.alertBody).getText(), ConfigFront.AlertMessage.submitedArticle);
	}
	
	
	@Test (description       = "Delete an article added with given alias then add it again",
			dependsOnMethods = { "loginRoot" })//, "addArticleSameName_GivenAlias" })
	public void addDeletedArticle_GivenAlias() {
		driver.get(ConfigAdmin.PageUrl.adminHome);		
		
		ArticleManagerPage articleManager = (new ControlPanelPage(driver)).gotoArticleManagerPage();
		articleManager.trashArticleByAlias(aliasA);
		
		assertTrue(driver.findElement(ConfigAdmin.alertBody).getText().contains(ConfigAdmin.AlertMessage.trashedArticle));
		
		// Go back to Submit Article Page
		driver.get(ConfigFront.PageUrl.submitArticle);
		
		// Add article with the same alias
		addArticleWithAlias(titleGiven, aliasA);		
		assertEquals(driver.findElement(ConfigFront.alertBody).getText(), ConfigFront.AlertMessage.errorSubmitArticle);
		joomlaSubmitArticle.clear();
	}
	
	
	// ************************************************************************
	// CONFIGURATION
	// ************************************************************************
	
	@BeforeClass
	public static void before() {
		// Create a new instance of a driver and navigate to the right place :D
		driver     = new FirefoxDriver();
		driver.get(ConfigFront.PageUrl.home);
		
		// Just some settings
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// Create a new instance of Joomla Home Page
		joomlaHome = PageFactory.initElements(driver, HomePage.class);
	}
	
	@AfterClass
	public static void after() throws InterruptedException {
		Thread.sleep(10000);
		driver.close();
		driver.quit();
	}
	
	public void addArticle(String title, String htmlContent, String frameContent) {
		joomlaSubmitArticle.typeTitle(title);
		
		joomlaSubmitArticle.typeHtmlText(htmlContent);
		joomlaSubmitArticle.typeHtmlText("<p>\n");
		joomlaSubmitArticle.typeFrameText(frameContent);
		
		joomlaSubmitArticle.save();
	}
	
	
	// already know that the textarea and the iframe editors are working
	// with this -> testing aliases behavior 
	public void addArticleWithAlias(String title, String alias) {
		joomlaSubmitArticle.typeTitle(title);
		joomlaSubmitArticle.typeAlias(alias);
		
		joomlaSubmitArticle.save();
	}

}
