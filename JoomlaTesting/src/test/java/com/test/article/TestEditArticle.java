package com.test.article;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.config.ConfigAdmin;
import com.config.ConfigFront;
import com.config.UserItem;
import com.joomla.admin.AdminLoginPage;
import com.joomla.admin.ControlPanelPage;
import com.joomla.admin.articles.ArticleEditPage;
import com.joomla.admin.articles.ArticleManagerPage;
import com.joomla.home.ArticleFormPage;
import com.joomla.home.ArticlePage;
import com.joomla.home.HomePage;

/**
 * <h2>Testing Joomla</h2>
 * <p> As the author, edit an article
 * <p> As a user, see that you cannot edit an article
 * <p> As an administrator, edit an article
 * 
 * @author Tase Gula
 */
public class TestEditArticle
{
	private static WebDriver driver;
	
	private static HomePage joomlaHome;
	private static ArticlePage latestArticle;
	private static ControlPanelPage controlPanel;
	
	// ************************************************************************
	// TESTS
	// ************************************************************************
	
	@Test (description = "Login as user with publishing rights - tase")
	public void loginAuthor() {
		driver.get(ConfigFront.PageUrl.home);
		joomlaHome.login(UserItem.TASE);
		
		// Hi, user.name();
		assertTrue(driver.findElement(By.xpath("//form[@id='login-form']/div[1]")).getText().contains(UserItem.TASE.name()));
	}
	
	@Test (description = "Login as user with no publishing rights - dummy")
	public void loginUser() {
		driver.get(ConfigFront.PageUrl.home);
		joomlaHome.login(UserItem.DUMMY);
		
		// Hi, user.name();
		assertTrue(driver.findElement(By.xpath("//form[@id='login-form']/div[1]")).getText().contains(UserItem.DUMMY.name()));
	}
	
	@Test (description = "Login administrator")
	public void loginRoot() {
		driver.get(ConfigAdmin.PageUrl.adminLogin);
		controlPanel = (new AdminLoginPage(driver)).login(ConfigAdmin.ROOT);
		
		assertTrue(driver.findElement(ControlPanelPage.contentArticleMg).isDisplayed());
	}
	
	@Test (description = "Go to the article page")
	public void gotoArticle() {
		String title = joomlaHome.getLatestArticleTitle();
		latestArticle = joomlaHome.gotoLatestArticle();
		
		assertEquals(latestArticle.getTitle(), title);
	}
	
	@Test (description       = "Test that the author have rights to edit",
			dependsOnMethods = { "loginAuthor", "gotoArticle" })
	public void authorEdit() {
		assertTrue(latestArticle.canEdit());
	}
	
	@Test (description = "Test that the registered user doesn't have rights to edit",
			dependsOnMethods = { "loginUser", "gotoArticle" })
	public void userEdit() {
		assertFalse(latestArticle.canEdit());
	}
	
	@Test (description       = "As the author, edit the article",
			dependsOnMethods = "authorEdit")
	public void editArticleByAuthor() {
		ArticleFormPage article = latestArticle.gotoEdit();
		article.clearBody();
		
		article.typeHtmlText("<h3> Sorry for the inconvenience </h3>\n");
		article.typeHtmlText("<p> We are going through some maintenance.\n");
		article.typeHtmlText("<p> But be sure that Ana still has her <b>mere</b>.\n<p>");
		
		article.save();
		
		assertEquals(driver.findElement(ConfigFront.alertBody).getText(), ConfigFront.AlertMessage.editedArticle);
	}
	
	@Test (description       = "As the administrator, edit the article",
			dependsOnMethods = "loginRoot")
	public void editArticleByAdmin() {
		ArticleManagerPage articleManager = controlPanel.gotoArticleManagerPage();
		int articleNo = 0;
		String title = articleManager.getTitle(articleNo);
		String extra = " albastre (Yeah! You heard me!)";
		
		ArticleEditPage article = articleManager.gotoArticle(articleNo);
		article.typeTitle(extra);
		article.save();
		
		driver.get(ConfigAdmin.PageUrl.contentArticleMg);
		assertEquals(articleManager.getTitle(articleNo), title + extra);
	}
	
	
	// ************************************************************************
	// CONFIGURATION
	// ************************************************************************
	
	@BeforeClass
	public static void before() {
		// Create a new instance of a driver and navigate to the right place :D
		driver     = new FirefoxDriver();
		driver.get(ConfigFront.PageUrl.home);
		
		// Just some settings
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// Create a new instance of Joomla Admin Page
		joomlaHome = PageFactory.initElements(driver, HomePage.class);
	}
	
	@AfterClass
	public static void after() throws InterruptedException {
		Thread.sleep(10000);
		driver.close();
		driver.quit();
	}
}
