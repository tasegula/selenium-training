package com.test.admin;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.config.ConfigAdmin;
import com.config.User;
import com.config.UserItem;
import com.joomla.admin.AdminLoginPage;
import com.joomla.admin.ControlPanelPage;
import com.joomla.admin.users.UserEditPage;
import com.joomla.admin.users.UserManagerPage;
import com.joomla.home.HomePage;

/**
 * <h2>Joomla - Testing User Manager</h2>
 * <p> An administrator logs in, activatates a registered user, and gives rights to another;
 * <p> This test depends on the Authentification tests being passed;
 * 
 * @author Tase Gula
 */
public class TestUserManager
{
	private static WebDriver driver;
	private static WebDriverWait waitDriver;
	
	private static AdminLoginPage joomlaAdminLogin;
	private static ControlPanelPage joomlaCpp;
	private static UserManagerPage joomlaUserMng;
	
	// ************************************************************************
	// DATA PROVIDERS
	// ************************************************************************
	@DataProvider(name = "testGiveRights")
	public Object[][] usersGiveRights() {
		return new Object[][] {
				{ UserItem.TASE },
			};
	}
	
	@DataProvider(name = "testUserManager")
	public Object[][] usersToggle() {
		return new Object[][] {
				{ UserItem.DUMMY },
			};
	}
	
	
	// ************************************************************************
	// TESTS
	// ************************************************************************
	
	@Test (description = "Admin logs in")
	public void loginRoot() {
		joomlaCpp = joomlaAdminLogin.login(ConfigAdmin.ROOT);
		assertEquals(driver.getCurrentUrl(), ConfigAdmin.PageUrl.adminHome);
	}

	
	@Test (description       = "Change to the User Manager Page",
			dependsOnMethods = { "loginRoot" })
	public void gotoUserManager() {
		joomlaUserMng = joomlaCpp.gotoUserManagerPage();
		assertEquals(driver.getCurrentUrl(), ConfigAdmin.PageUrl.userManager);
	}

	
	@Test (description       = "Activate a registered user from the administrator's page",
			dataProvider     = "testUserManager",
			dependsOnMethods = { "gotoUserManager" })
	public void activateUser(User user) {
		joomlaUserMng.activateUser(user.username());
		assertTrue(driver.findElement(UserManagerPage.activatedLoc).getAttribute("data-original-title").equals("Activated"));
	}

	
	@Test (description       = "Enable a registered user from the administrator's page",
			dataProvider     = "testUserManager",
			dependsOnMethods = { "gotoUserManager" })
	public void enableUser(User user) {
		joomlaUserMng.enableUser(user.username());
		
		waitDriver.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='alert alert-success']")));
		assertTrue(driver.findElement(UserManagerPage.enabledLoc).getAttribute("data-original-title").contains("Block"));
	}
	
	
	@Test (description       = "Login the user which was activated and unblocked by an administrator",
			dataProvider     = "testUserManager",
			dependsOnMethods = { "activateUser", "enableUser" })
	public void loginActivatedUser(User user) {
		// try to login
		HomePage joomlaHomePage = new HomePage(driver);
		joomlaHomePage.login(user);
		
		// Hi, user.name()
		assertTrue(driver.findElement(By.xpath("//form[@id='login-form']/div[1]")).getText().contains(user.name()));
	}
	
	
	@Test (description       = "Give rights to publish/edit articles to an activated user",
			dataProvider     = "testGiveRights",
			dependsOnMethods = { "gotoUserManager" })
	public void giveRights(User user) {
		UserEditPage editUser = joomlaUserMng.gotoEditUser(user.username());
		editUser.gotoAssignedUserGroups();
		
		UserEditPage.AssignedUserGroups.checkEditorGroup();
		UserEditPage.AssignedUserGroups.checkAuthorGroup();
		UserEditPage.AssignedUserGroups.checkPublisherGroup();
		UserEditPage.AssignedUserGroups.uncheckRegisteredGroup();
		
		editUser.saveAndClose();
		assertEquals(driver.getCurrentUrl(), ConfigAdmin.PageUrl.userManagerFromEdit);
	}
	
	
	// ************************************************************************
	// CONFIGURATION
	// ************************************************************************
	
	@BeforeClass
	public void before() {
		// Create a new instance of a driver and navigate to the right place :D
		driver     = new FirefoxDriver();
		waitDriver = new WebDriverWait(driver, 10);
		driver.get(ConfigAdmin.PageUrl.adminLogin);
		
		// Just some settings
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// Create a new instance of Joomla Admin Page
		joomlaAdminLogin = PageFactory.initElements(driver, AdminLoginPage.class);
	}
	
	@AfterClass
	public void after() {
		driver.close();
		driver.quit();
	}
}
