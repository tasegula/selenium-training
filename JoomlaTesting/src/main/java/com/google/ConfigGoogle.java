package com.google;

import org.openqa.selenium.By;

/**
 * <h2>Joomla - Google</h2>
 * <p> Configuration Stuff
 * 
 * @author Tase Gula
 */
public class ConfigGoogle
{
	/**
	 * Some Gmail locators
	 */
	public static final class AccountsLocator
	{
		public static final By profile        = By.xpath(".//*[@class='gb_y gb_4 gb_e']");
	}
	
	public static final class GmailLocator
	{
		public static final By backToInbox = By.xpath("//div[@aria-label='Back to Inbox']");
		public static final By delete      = By.xpath("//div[@aria-label='Delete']/div");
		public static final By signOut     = By.id("gb_71");
	}
}
