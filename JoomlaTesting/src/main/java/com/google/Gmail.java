package com.google;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * <h2>Joomla - Google</h2>
 * <p> Gmail
 * 
 * @author Tase Gula
 */
public class Gmail
{
	private static final String url = "https://mail.google.com";
	private final WebDriver driver;

	// links	
	By composeButtonLocator = By.xpath("//div[@class='aic']/div[@class='z0']/div[@role='button']");
	By toInputLocator       = By.name("to");
	By subjectInputLocator  = By.name("subjectbox");
	By messageInputLocator  = By.xpath(" //div/iframe");
	By sendButtonLocator    = By.xpath("//div[@class='J-J5-Ji']/div[@role='button']");
	
	By allMailsLocator  = By.xpath("//div[@class='Cp']/div/table/tbody/tr");
	By mailTitleLocator = By.xpath("./td[6]/div/div/div/span[1]");
	
	public Gmail(WebDriver driver) {
		this.driver = driver;
		driver.get(url);
	}
	
	public boolean goToMail(String title) {
		List<WebElement> mails = driver.findElements(allMailsLocator);
		
		for (WebElement mail : mails) {
			WebElement auxLink = mail.findElement(mailTitleLocator);
			if (title.equals(auxLink.getText())) {
				auxLink.click();
				return true;
			}
		}
		
		return false;
	}
	
	
	// ************************************************************************
	// Send Email Stuff
	// ************************************************************************
	
	public Gmail GoToNewMailForm (){
		this.driver.findElement(composeButtonLocator).click();
		return this;
	}
	
	public Gmail typeTo(String content) {
		driver.findElement(toInputLocator).sendKeys(content);
		return this;    
	}
	
	public Gmail typeSubject(String content) {
		driver.findElement(subjectInputLocator).sendKeys(content);
		return this;    
	}
	
	public Gmail typeMessage(String content) {
		driver.findElement(messageInputLocator).sendKeys(content);
		return this;    
	}
	
	public Gmail sendEmail(String to, String subject, String message) {
		typeTo(to);
		typeSubject(subject);
		typeMessage(message);
		driver.findElement(sendButtonLocator).click();
		return this;    
	}
	
	public Gmail deleteCurrentEmail() {
		driver.findElement(ConfigGoogle.GmailLocator.delete).click();		
		return this;
	}
}
