package com.google;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * <h2>Joomla - Google</h2>
 * <p> Google Account Page (Redirected)
 * 
 * @author Tase Gula
 */
public class GoogleLogin
{
	private static final String url = "https://accounts.google.com";
	private final WebDriver driver;
	
	// links	
	By usernameLocator = By.id("Email");
	By passwordLocator = By.id("Passwd");
	By submitLocator   = By.id("signIn");
	By menuLocator     = By.xpath("//div[@class='gb_Ta']/a");
	By gmailLocator    = By.id("gb23");
	
	public GoogleLogin(WebDriver driver) {
		this.driver     = driver;
		driver.get(url);
	}
	
	
	public GoogleLogin login(String username, String password) {
		this.driver.findElement(usernameLocator).sendKeys(username);
		this.driver.findElement(passwordLocator).sendKeys(password);
		this.driver.findElement(submitLocator).click();
		return this;
	}
	
	public Gmail gotoGmail() {
		this.driver.findElement(menuLocator).click();
		this.driver.findElement(gmailLocator).click();
		return new Gmail(this.driver);
	}
}
