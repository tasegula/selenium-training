package com.config;

/**
 * <h2>Joomla - Generic User class</h2>
 * 
 * @author Tase Gula
 */
public class User
{
	private String name;
	private String username;

	private String password;
	private String comfPass;

	private String mailUser;
	private String comfMail;
	private String mailPass;

	protected User(
			String name,
			String username,
			String password,
			String comfPass,
			String mailUser,
			String comfMail,
			String mailPass)
	{
		this.name     = name;
		this.username = username;
		this.password = password;
		this.comfPass = comfPass;
		this.mailUser = mailUser;
		this.comfMail = comfMail;
		this.mailPass = mailPass;
	}

	public String name() {
		return this.name;
	}

	public String username() {
		return this.username;
	}

	public String password() {
		return this.password;
	}

	public String comfPass() {
		return this.comfPass;
	}

	public String mailUser() {
		return this.mailUser;
	}

	public String comfMail() {
		return this.comfMail;
	}

	public String mailPass() {
		return this.mailPass;
	}
	
	public String toString() {
		return name + ": "
			+ username + "/" + password + "=" + comfPass + " -> "
			+ mailUser + "/" + mailPass + "=" + comfMail + " -> ";
	}
}
