package com.config;

/**
 * <h2>Joomla - Specific (static) Users class</h2>
 * 
 * @author Tase Gula
 */
public class UserItem
{
	public static String alternativeUser = "Java";
	public static String alternativePass = "1234567890";
	
	
	// Credentials for one user - Tase
	private static final class UserTase extends User
	{
		
		static final String name = "Tase Joomla";
		static final String username = "tase";
		static final String password = "tase01234";
		static final String mailUser = "test.tase@gmail.com";
		static final String mailPass = "tase01234";
		
		public UserTase() {
			super(name, username, password, password, mailUser, mailUser, mailPass);
		}
	}
	
	// Credentials for one user - David
	private static final class UserDavid extends User
	{
		
		static final String name = "David Joomla";
		static final String username = "david";
		static final String password = "david01234";
		static final String mailUser = "test.david@gmail.com";
		static final String mailPass = "david01234";
		
		public UserDavid() {
			super(name, username, password, password, mailUser, mailUser, mailPass);
		}
	}
	
	// Credentials for one user -> never registered
	private static final class UserFoo extends User
	{
		static final String name     = "Foo User";
		static final String username = "foo";
		static final String password = "foo01234";
		static final String mailUser = "test.dummy@gmail.com";
		static final String mailPass = "foo01234";
		
		public UserFoo() {
			super(name, username, password, password, mailUser, mailUser, mailPass);
		}
		
		static final String wrongPass = "dummy01234";
		static final String wrongMail = "foo_test_dummy@gmail.com";
	}
	
	// Credentials for one user -> never registered
	private static final class UserDummy extends User
	{
		static final String name     = "Dummy User";
		static final String username = "dummy";
		static final String password = "dummy01234";
		static final String mailUser = "dummy@dummy.dummy";
		static final String mailPass = "dummy01234";
		
		public UserDummy() {
			super(name, username, password, password, mailUser, mailUser, mailPass);
		}
	}

	
	// Registered USERS
	public static final User TASE  = new UserTase();	
	public static final User DAVID = new UserDavid();
	
	
	// User registered, but not activated; Dummy credentials lost
	public static final User DUMMY = new User(UserTase.name,		// Same as TASE
											  UserDummy.username,
											  UserDummy.password, 
											  UserDummy.password, 
											  UserDummy.mailUser, 
											  UserDummy.mailUser, 
											  UserDummy.mailPass);
	
	// User unregistered; Foo credentials available
	public static final User DUP_USER = new User(UserFoo.name,
												 UserDavid.username,	// Same as DAVID
												 UserFoo.password, 
												 UserFoo.password, 
												 UserFoo.mailUser, 
												 UserFoo.mailUser, 
												 UserFoo.mailPass);
	
	// User unregistered; Foo credentials available
	public static final User DUP_MAIL = new User(UserFoo.name,
												 UserFoo.username,
												 UserFoo.password, 
												 UserFoo.password, 
												 UserDavid.mailUser, 
												 UserDavid.mailUser, 
												 UserDavid.mailPass);

	// User unregistered; confirmation password wrong
	public static final User COMF_PASS = new User(UserFoo.name, 
												  UserFoo.username,
												  UserFoo.password, 
												  UserFoo.wrongPass, 
												  UserFoo.mailUser, 
												  UserFoo.mailUser, 
												  UserFoo.mailPass);
	
	// User unregistered; confirmation mail wrong
	public static final User COMF_MAIL = new User(UserFoo.name, 
												  UserFoo.username,
												  UserFoo.password, 
												  UserFoo.password, 
												  UserFoo.mailUser, 
												  UserFoo.wrongMail, 
												  UserFoo.mailPass);
}
