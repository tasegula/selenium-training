package com.config;

import org.openqa.selenium.By;


/**
 * <h2>Joomla - Configuration Stuff</h2>
 * <p> The Administrator Website
 * 
 * @author Tase Gula
 */
public class ConfigAdmin
{
	// some things are better off ungrouped
	public static final String siteName = "Testing Stuff";
	
	public static final String rootName     = "Tase Root";
	public static final String rootUsername = "root";
	public static final String rootPassword = "123456";
	public static final String rootMail = "root.mail@real.com";
	
	public static final String mailUser = "user.mail@real.com";
	public static final String mailPass = "123456";
	
	
	// ROOT USER
	public static final User ROOT = new User(ConfigAdmin.rootName,
											 ConfigAdmin.rootUsername,
											 ConfigAdmin.rootPassword,
											 ConfigAdmin.rootPassword,
											 ConfigAdmin.mailUser,
											 ConfigAdmin.mailUser,
											 ConfigAdmin.mailPass);
	
	
	// MySql Credentials
	public static final class MySql
	{
		public static final String username = "root";
		public static final String password = "123456789";
	}
	
	
	// Edit Pages saving options
	public static final class Edit
	{
		public static final By save      = By.xpath("//div[@id='toolbar-apply']/button");
		public static final By saveClose = By.xpath("//div[@id='toolbar-save']/button");
		public static final By saveNew   = By.xpath("//div[@id='toolbar-save-new']/button");
		public static final By saveCopy  = By.xpath("//div[@id='toolbar-save-copy']/button");
		public static final By close     = By.xpath("//div[@id='toolbar-cancel']/button");
	}
	
	
	// Pages
	public static final class PageUrl
	{
		public static final String adminLogin = "http://localhost/joomla/administrator/";
		public static final String adminHome  = "http://localhost/joomla/administrator/index.php";
		
		public static final String contentAddArticle = "http://localhost/joomla/administrator/index.php?option=com_content&view=article&layout=edit";
		public static final String contentArticleMg  = "http://localhost/joomla/administrator/index.php?option=com_content";
		public static final String contentCategoryMg = "http://localhost/joomla/administrator/index.php?option=com_categories&extension=com_content";
		public static final String contentMediaMg    = "http://localhost/joomla/administrator/index.php?option=com_media";
		
		public static final String structMenuMg   = "http://localhost/joomla/administrator/index.php?option=com_menus";
		public static final String structModuleMg = "http://localhost/joomla/administrator/index.php?option=com_modules";
		
		public static final String userManager         = "http://localhost/joomla/administrator/index.php?option=com_users";
		public static final String userManagerFromEdit = "http://localhost/joomla/administrator/index.php?option=com_users&view=users";
		
		public static final String configGlobal   = "http://localhost/joomla/administrator/index.php?option=com_config";
		public static final String configTemplate = "http://localhost/joomla/administrator/index.php?option=com_templates";
		public static final String configLanguage = "http://localhost/joomla/administrator/index.php?option=com_languages";
	}
	
	// Alert messages -> new category; may not be used everywhere// 
	public static final By alertHead  = By.xpath("//div[@id='system-message-container']/div/h4");
	public static final By alertBody  = By.xpath("//div[@id='system-message-container']/div/p");
	public static final By alertClose = By.xpath("//div[@id='system-message-container']/button");
	
	public static final class AlertMessage
	{
		public static final String trashedArticle  = "article trashed.";
		public static final String trashedArticles = "articles trashed.";
		
		public static final String login = "You do not have access to the administrator section of this site.";
	}
}
