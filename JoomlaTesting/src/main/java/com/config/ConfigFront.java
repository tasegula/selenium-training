package com.config;

import org.openqa.selenium.By;

/**
 * <h2>Joomla - Configuration Stuff</h2>
 * <p> The Front Website
 * 
 * @author Tase Gula
 */
public final class ConfigFront
{
	// Pages
	public static final class PageUrl
	{
		public static final String home  = "http://localhost/joomla/";
		public static final String login = "http://localhost/joomla/index.php/component/users/?view=login";
		
		public static final String createAccount  = "http://localhost/joomla/index.php/component/users/?view=registration";
		public static final String createdAccount = "http://localhost/joomla/index.php/component/users/?view=login";
		
		public static final String forgotUsername  = "http://localhost/joomla/index.php/component/users/?view=remind";
		public static final String forgotPassword  = "http://localhost/joomla/index.php/component/users/?view=reset";
		public static final String recoverPassword = "http://localhost/joomla/index.php/component/users/?view=reset&layout=complete";
	
		public static final String profile       = "http://localhost/joomla/index.php/your-profile";		
		public static final String submitArticle = "http://localhost/joomla/index.php/submit-an-article";
		public static final String submitWeblink = "http://localhost/joomla/index.php/submit-a-weblink";
	}
	
	// TextFields used across the platform
	public static final class Form
	{
		public static final By name     = By.id("jform_name");
		public static final By username = By.id("jform_username");
		public static final By password = By.id("jform_password1");
		public static final By comfPass = By.id("jform_password2");
		public static final By email    = By.id("jform_email1");
		public static final By comfMail = By.id("jform_email2");
		
		public static final By simpleUser = By.id("username");
		public static final By simplePass = By.id("password");
		public static final By simpleMail = By.id("jform_email");
	}
	
	// Login form on all pages
	public static final class MenuLogin
	{
		public static final By formLoc = By.id("login-form");
		
		public static final By usernameLoc    = By.id("modlgn-username");
		public static final By passwordLoc    = By.id("modlgn-passwd");
		public static final By rememberLoc    = By.id("modlgn-remember");
		public static final By loginButtonLoc = By.xpath("//button[@class='btn btn-primary']");
		
		public static final By createAccountLoc  = By.xpath("//ul[@class='unstyled']/li[1]/a");
		public static final By forgotUsernameLoc = By.xpath("//ul[@class='unstyled']/li[2]/a");
		public static final By forgotPasswordLoc = By.xpath("//ul[@class='unstyled']/li[3]/a");
		
		public static final By logoutButtonLoc = By.xpath("//input[@value='Log out']");
	}
	
	// User Menu on all pages
	public static final class MenuUser
	{
		public static final By profileLoc       = By.xpath("//li[@class='item-102']/a");
		public static final By submitArticleLoc = By.xpath("//li[@class='item-104']/a");
		public static final By submitWeblinkLoc = By.xpath("//li[@class='item-105']/a");
	}
	
	// Alert messages -> new category; may not be used everywhere// 
	public static final By alertHead  = By.xpath("//div[@class='system-message']/div/h4");
	public static final By alertBody  = By.xpath("//div[@id='system-message']/div/div");
	public static final By alertClose = By.xpath("//div[@class='system-message']/div/a");
	
	public static final class AlertMessage
	{
		public static final String loginError = "Login denied! Your account has either been blocked or you have not activated it yet.";
		public static final String loginWarn  = "Username and password do not match or you do not have an account yet.";
		
		public static final String createAccountUsernameInUse = "The username you entered is not available. Please pick another username.";
		public static final String createAccountMailInUse     = "The email address you entered is already in use or invalid. Please enter another email address.";
		public static final String createAccountPassowrdMatch = "The passwords you entered do not match. Please enter your desired password in the password field and confirm your entry by entering it in the confirm password field.";
		public static final String createAccountMailMatch     = "The email addresses you entered do not match. Please enter your email address in the email address field and confirm your entry by entering it in the confirm email field.";
		
		
		public static final String submitedArticle    = "Article successfully submitted";
		public static final String errorSubmitArticle = "Save failed with the following error: Another article from this category has the same alias";
		public static final String editedArticle      = "Article successfully saved";
	}
}
