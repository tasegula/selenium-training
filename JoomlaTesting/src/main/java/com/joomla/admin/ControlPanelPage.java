package com.joomla.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.config.ConfigAdmin;
import com.joomla.admin.articles.ArticleManagerPage;
import com.joomla.admin.users.UserManagerPage;

/**
 * <h2>Joomla - Administrator Site</h2>
 * <p> Control Panel (Home) Page<
 * 
 * @author Tase Gula
 */
public class ControlPanelPage
{
	private final String url = ConfigAdmin.PageUrl.adminHome;
	private final WebDriver driver;
	
	// dropdown menu; used for logout
	public static final By dropMenuLoc = By.xpath("//ul[@class='nav nav-user pull-right']/li");
	
	// using will return link-buttons to the given pages
	public static final By contentAddArticle = By.xpath("//span[@class='j-links-link' and text()='Add New Article']");
	public static final By contentArticleMg  = By.xpath("//span[@class='j-links-link' and text()='Article Manager']");
	public static final By contentCategoryMg = By.xpath("//span[@class='j-links-link' and text()='Category Manager']");
	public static final By contentMediaMg    = By.xpath("//span[@class='j-links-link' and text()='Media Manager']");
	
	public static final By userManager = By.xpath("//span[@class='j-links-link' and text()='User Manager']");
	
	// using it will return a List<WebElement>
	public static final By loggedInUsers   = By.xpath("//*[@class='well well-small' and contains(., 'Logged-in Users')]");
	public static final By popularArticles = By.xpath("//*[@class='well well-small' and contains(., 'Popular Articles')]");
	public static final By recentlyAdded   = By.xpath("//*[@class='well well-small' and contains(., 'Recently Added Articles')]");
	
	public ControlPanelPage(WebDriver driver) {
		this.driver = driver;
		
		if (!url.equals(driver.getCurrentUrl())) {
			System.err.println(this.getClass().getName() + " initialized on: " + driver.getCurrentUrl());
		}
	}
	
	
	public AdminLoginPage logout() {
		WebElement dropDownListBox = driver.findElement(dropMenuLoc);
		Select clickThis = new Select(dropDownListBox);
		clickThis.selectByValue("Log out");
		
		return new AdminLoginPage(this.driver);
	}
	
	
	public UserManagerPage gotoUserManagerPage() {
		this.driver.findElement(userManager).click();
		return new UserManagerPage(this.driver);
	}
	
	public ArticleManagerPage gotoArticleManagerPage() {
		this.driver.findElement(contentArticleMg).click();
		return new ArticleManagerPage(this.driver);
	}
}
