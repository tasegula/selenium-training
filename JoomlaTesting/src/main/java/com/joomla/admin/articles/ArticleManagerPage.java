package com.joomla.admin.articles;

import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.config.ConfigAdmin;

/**
 * <h2>Joomla - Administrator Site</h2>
 * <p> Article Manager Page
 * 
 * @author Tase Gula
 */
public class ArticleManagerPage
{
	private final String url = ConfigAdmin.PageUrl.userManager;
	private final WebDriver driver;
	
	// return the List<WebElement> of added articles
	public static final By allArticlesLocator = By.xpath("//table[@class='table table-striped']/tbody/tr");
	
	// elements for each Article from the Article Table;
	public static final By titleLoc    = By.xpath(".//a[@title='Edit']");
	public static final By aliasLoc    = By.xpath(".//div[@class='pull-left']/span");
	
	public static final By checkLoc    = By.xpath(".//input[@id='cb0']");
	public static final By publishLoc  = By.xpath(".//div[@class='btn-group']/a[1]");
	public static final By featuredLoc = By.xpath(".//div[@class='btn-group']/a[2]");
	public static final By idLoc       = By.xpath(".//td[10]");
	
	public static final By archiveLoc  = By.xpath(".//div[@id='toolbar-archive']/button");
	public static final By trashLoc    = By.xpath(".//div[@id='toolbar-trash']/button");
	
	
	public ArticleManagerPage(WebDriver driver) {		
		this.driver = driver;
		
		if(!url.equals(driver.getCurrentUrl())) {
			System.err.println(this.getClass().getName() + " initialized on: " + driver.getCurrentUrl());
		}
	}
	
	// delete the article with the given title
	public ArticleManagerPage trashArticleByTitle(String title) {
		List<WebElement> articles = driver.findElements(allArticlesLocator);
		
		for (WebElement article: articles) {
			if (title.equals(article.findElement(titleLoc).getText())) {
				driver.findElement(checkLoc).click();
				driver.findElement(trashLoc).click();
				return this;
			}
		}

		throw new NoSuchElementException("There is no article with title=" + title);
	}
	
	
	// delete the article with the given alias
	public ArticleManagerPage trashArticleByAlias(String alias) {
		List<WebElement> articles = driver.findElements(allArticlesLocator);
		
		for (WebElement article: articles) {
			String fieldAlias  = article.findElement(aliasLoc).getText();
			String aux         = (fieldAlias.split(": "))[1];
			String actualAlias = (aux.split("\\)"))[0];
			
			if (alias.equals(actualAlias)) {
				driver.findElement(checkLoc).click();
				driver.findElement(trashLoc).click();
				return this;
			}
		}

		throw new NoSuchElementException("There is no article with alias=" + alias);
	}
	
	
	public ArticleEditPage gotoEditArticleByAlias(String alias) {
		List<WebElement> articles = driver.findElements(allArticlesLocator);
		
		for (WebElement article: articles) {
			String fieldAlias  = article.findElement(aliasLoc).getText();
			String aux         = (fieldAlias.split(": "))[1];
			String actualAlias = (aux.split("\\)"))[0];
			
			if (alias.equals(actualAlias)) {
				String id = driver.findElement(idLoc).getText();
				
				driver.findElement(titleLoc).click();
				return new ArticleEditPage(driver, id);
			}
		}

		throw new NoSuchElementException("There is no article with alias=" + alias);
	}
	
	
	// returns the alias of the article with the given title
	public String getAlias(String title) {
		List<WebElement> articles = driver.findElements(allArticlesLocator);
		
		for (WebElement article: articles) {
			if (title.equals(article.findElement(titleLoc).getText())) {
				String alias = driver.findElement(aliasLoc).getText();
				// [0]='alias', [1]=$(alias)
				String aux = (alias.split(": "))[1];
				return (aux.split("\\)"))[0];
			}
		}

		throw new NoSuchElementException("There is no article with title=" + title);
	}
	
	// returns the ArticleEditPage for the given article
	public ArticleEditPage gotoArticle(int i) {
		WebElement article = driver.findElements(allArticlesLocator).get(i);
		String id = article.findElement(idLoc).getText();
		
		article.findElement(titleLoc).click();		
		return new ArticleEditPage(driver, id);
	}
	
	// returns the Title for the given article
	public String getTitle(int i) {
		WebElement article = driver.findElements(allArticlesLocator).get(i);
		return article.findElement(titleLoc).getText();
	}

}
