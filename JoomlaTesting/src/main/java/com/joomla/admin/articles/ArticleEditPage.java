package com.joomla.admin.articles;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.config.ConfigAdmin;


/**
 * <h2>Joomla - Administrator Site</h2>
 * <p> Article Edit Page
 * 
 * @author Tase Gula
 */
public class ArticleEditPage
{
	private static String url = "http://localhost/joomla/administrator/index.php?option=com_content&view=article&layout=edit&id=";
	private static WebDriver driver;
	
	public ArticleEditPage(WebDriver driver, String id) {
		url += id;
		ArticleEditPage.driver = driver;
		
		if (!url.equals(driver.getCurrentUrl())) {
			System.err.println(this.getClass().getName() + " initialized on: " + driver.getCurrentUrl());
		}
	}
	
	
	public ArticleEditPage save() {
		driver.findElement(ConfigAdmin.Edit.save).click();
		return this;
	}

	public ArticleEditPage saveAndNew() {
		driver.findElement(ConfigAdmin.Edit.saveNew).click();
		return this;
	}
	
	public ArticleEditPage saveAsCopy() {
		driver.findElement(ConfigAdmin.Edit.saveCopy).click();
		return this;
	}

	public ArticleManagerPage saveAndClose() {
		driver.findElement(ConfigAdmin.Edit.saveClose).click();
		return new ArticleManagerPage(driver);
	}
	
	public ArticleManagerPage close() {
		driver.findElement(ConfigAdmin.Edit.close).click();
		return new ArticleManagerPage(driver);
	}
	
	
	// ************************************************************************
	// CONTENT TAB CLASS + METHODS
	// ************************************************************************
	
	public static final class ContentTab
	{
		// title and alias
		public static final By titleLoc = By.id("jform_title");
		public static final By aliasLoc = By.id("jform_alias");
		
		// editor types
		public static final By htmlEditorLoc  = By.id("jform_articletext");
		public static final By frameEditorLoc = By.id("jform_articletext_ifr");
		
		// footer buttons
		public static final By articleButtonLoc   = By.xpath("//a[@class='btn modal-button' and @title='Article']");
		public static final By imageButtonLoc     = By.xpath("//a[@class='btn modal-button' and @title='Image']");
		public static final By pageBreakButtonLoc = By.xpath("//a[@class='btn modal-button' and @title='Page Break']");
		public static final By toggleButtonloc    = By.xpath("//div[@class='toggle-editor btn-toolbar pull-right clearfix']/div/a");
		
		// dropdown lists
		public static final By categoryLoc = By.id("jform_catid_chzn");
		public static final By statusLoc   = By.id("jform_state_chzn");
		public static final By accessLoc   = By.id("jform_access_chzn");
		
		// feature status
		public static final By featuresYesLoc = By.xpath("//label[@for='jform_featured0']");
		public static final By featuresNoLoc  = By.xpath("//label[@for='jform_featured1']");
		
		// tags locators
		public static final By tagsInputLoc = By.xpath("//div[@id='jform_tags_chzn']/ul/li/input");
		
		public static final By tagsLoc      = By.xpath("//div[@id='jform_tags_chzn']/ul/li");
		public static final By tagTextLoc   = By.xpath(".//span");
		public static final By tagDeleteLoc = By.xpath(".//a");
		
		
		private ContentTab() {}
	}
	
	// insert the content in the TITLE field
	public ArticleEditPage typeTitle(String content) {
		driver.findElement(ContentTab.titleLoc).sendKeys(content);
		return this;
	}
	
	// insert the content in the ALIAS field
	public ArticleEditPage typeAlias(String content) {
		driver.findElement(ContentTab.aliasLoc).sendKeys(content);
		return this;
	}
	
	// insert the content in the TEXT field, using HTML editor
	public ArticleEditPage typeHtmlText(String content) {
		WebElement toggleEditor = driver.findElement(ContentTab.toggleButtonloc);
		WebElement simpleEditor = driver.findElement(ContentTab.htmlEditorLoc);
		
		toggleEditor.click();
		simpleEditor.sendKeys(content);
		toggleEditor.click();
		return this;
	}
	
	// because this project has a didactic purpose, this method is using the iframe editor
	public ArticleEditPage typeFrameText(String content) {
		driver.switchTo().frame(driver.findElement(ContentTab.frameEditorLoc));
		driver.findElement(By.xpath("//body")).sendKeys(content + "\n");
		driver.switchTo().defaultContent();
		return this;
	}
	
	// insert the given contents as TAGS
	public ArticleEditPage typeTags(List<String> tags) {
		for (String tag: tags) {
			driver.findElement(ContentTab.tagsInputLoc).sendKeys(tag + "\n");
		}
		
		return this;
	}
	
	// return a LIST of TAGS from the article
	public List<String> getTags() {
		List<WebElement> tags = driver.findElements(ContentTab.tagsLoc);
		List<String> tagsList = new ArrayList<String>();
		
		for (WebElement tag: tags) {
			tagsList.add(tag.getText());
		}
		
		return tagsList;
	}
	
	// choose the given CATEGORY
	public ArticleEditPage setCategory(String choice) {
		return setProperty(ContentTab.categoryLoc, choice);
	}
	
	// choose the given STATUS
	public ArticleEditPage setStatus(String choice) {
		return setProperty(ContentTab.statusLoc, choice);
	}
	
	// choose the given ACCESS
	public ArticleEditPage setAccess(String choice) {
		return setProperty(ContentTab.accessLoc, choice);
	}
	
	// helper for choosing the given CATEGORY, STATUS or ACCESS
	private ArticleEditPage setProperty(By property, String choice) {
		WebElement dropDownListBox = driver.findElement(property);
		Select clickThis = new Select(dropDownListBox);
		clickThis.selectByValue(choice);
		
		return this;
	}
	
	// empty all input fields
	public ArticleEditPage clear() {
		driver.findElement(ContentTab.titleLoc).clear();
		driver.findElement(ContentTab.aliasLoc).clear();
		driver.findElement(ContentTab.titleLoc).clear();
		
		WebElement toggleEditor = driver.findElement(ContentTab.toggleButtonloc);
		WebElement simpleEditor = driver.findElement(ContentTab.htmlEditorLoc);
		
		toggleEditor.click();
		simpleEditor.clear();
		toggleEditor.click();
		
		List<WebElement> tags = driver.findElements(ContentTab.tagsLoc);
		for (WebElement tag: tags) {
			tag.findElement(ContentTab.tagDeleteLoc).click();
		}
		
		return this;
	}
	
}
