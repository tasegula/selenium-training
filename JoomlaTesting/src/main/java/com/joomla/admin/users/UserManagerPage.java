package com.joomla.admin.users;

import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.config.ConfigAdmin;

/**
 * <h2>Joomla - Administrator Site</h2>
 * <p> User Manager Page
 * 
 * @author Tase Gula
 */
public class UserManagerPage
{
	private final String url = ConfigAdmin.PageUrl.userManager;
	private final WebDriver driver;
	
	// return the List<WebElement> of registered users (activated or not)
	public static final By allUsersLocator = By.xpath("//table[@class='table table-striped']/tbody/tr");
	
	// elements for each User from the User Table; for some of them, td[] is the only option
	public static final By checkLoc     = By.xpath(".//td[1]/input");
	public static final By editLoc      = By.xpath(".//td[2]/a");
	public static final By usernameLoc  = By.xpath(".//td[3]");
	public static final By enabledLoc   = By.xpath(".//td[4]/a");
	public static final By activatedLoc = By.xpath(".//td[5]/a");
	public static final By groupLoc     = By.xpath(".//td[6]");
	public static final By emailLoc     = By.xpath(".//td[7]");
	public static final By IdLoc        = By.xpath(".//td[10]");
	
	
	public UserManagerPage(WebDriver driver) {		
		this.driver = driver;
		
		if(!url.equals(driver.getCurrentUrl())) {
			System.err.println(this.getClass().getName() + " initialized on: " + driver.getCurrentUrl());
		}
	}
	
	
	public UserEditPage gotoEditUser(String username) {
		List<WebElement> users = driver.findElements(allUsersLocator);
		int id = 0;
		
		System.out.println(username);
		
		for (WebElement user: users) {
			if (username.equals(user.findElement(usernameLoc).getText())) {
				id = Integer.parseInt(user.findElement(IdLoc).getText());
				
				user.findElement(editLoc).click();
				return new UserEditPage(driver, id);
			}
		}
		
		throw new NoSuchElementException("There is no user with username=" + username);
	}
	
	public UserManagerPage enableUser(String username) {
		List<WebElement> users = driver.findElements(allUsersLocator);
		
		for (WebElement user: users) {
			if (username.equals(user.findElement(usernameLoc).getText())) {
				WebElement toggle = user.findElement(enabledLoc);
				
				if (toggle.getAttribute("data-original-title").contains("Unblock")) {
					toggle.click();
				}
				return this;
			}
		}

		throw new NoSuchElementException("There is no user with username=" + username);
	}
	
	public UserManagerPage blockUser(String username) {
		List<WebElement> users = driver.findElements(allUsersLocator);
		
		for (WebElement user: users) {
			if (username.equals(user.findElement(usernameLoc).getText())) {
				WebElement toggle = user.findElement(enabledLoc);
				
				if (toggle.getAttribute("data-original-title").contains("Block")) {
					toggle.click();
				}
				return this;
			}
		}

		throw new NoSuchElementException("There is no user with username=" + username);
	}
	
	public UserManagerPage activateUser(String username) {
		List<WebElement> users = driver.findElements(allUsersLocator);
		
		for (WebElement user: users) {
			if (username.equals(user.findElement(usernameLoc).getText())) {
				user.findElement(activatedLoc).click();
				return this;
			}
		}

		throw new NoSuchElementException("There is no user with username=" + username);
	}
	
	public String getMailForUser(String username) {
		List<WebElement> users = driver.findElements(allUsersLocator);
		
		for (WebElement user: users) {
			if (username.equals(user.findElement(usernameLoc).getText())) {
				return user.findElement(emailLoc).getText();
			}
		}
		
		throw new NoSuchElementException("There is no user with username=" + username);
	}
	
	public String getGroupForUser(String username) {
		List<WebElement> users = driver.findElements(allUsersLocator);
		
		for (WebElement user: users) {
			if (username.equals(user.findElement(usernameLoc).getText())) {
				return user.findElement(groupLoc).getText();
			}
		}
		
		throw new NoSuchElementException("There is no user with username=" + username);
	}
	
}
