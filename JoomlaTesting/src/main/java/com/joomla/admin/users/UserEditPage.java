package com.joomla.admin.users;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.config.ConfigAdmin;

/**
 * <h2>Joomla - Administrator Site</h2>
 * <p> User Edit Page
 * 
 * @author Tase Gula
 */
public class UserEditPage
{
	private static String url;
	private static WebDriver driver;
	
	// switch between tabs
	public static final By accountDetails     = By.xpath("//ul[@id='myTabTabs']/li[1]/a");
	public static final By assignedUserGroups = By.xpath("//ul[@id='myTabTabs']/li[2]/a");
	public static final By basicSettings      = By.xpath("//ul[@id='myTabTabs']/li[3]/a"); // is not used
	
	
	public UserEditPage(WebDriver driver, int id) {
		url = "http://localhost/joomla/administrator/index.php?option=com_users&view=user&layout=edit&id=" + id;
		UserEditPage.driver = driver;
		
		if (!url.equals(driver.getCurrentUrl())) {
			System.err.println(this.getClass().getName() + " initialized on: " + driver.getCurrentUrl());
		}
	}
	
	public UserEditPage gotoAccountDetails() {
		driver.findElement(accountDetails).click();		
		return this;
	}
	
	public UserEditPage gotoAssignedUserGroups() {
		driver.findElement(assignedUserGroups).click();	
		return this;
	}
	
	public UserEditPage save() {
		driver.findElement(ConfigAdmin.Edit.save).click();
		return this;
	}
	
	public UserEditPage saveAndNew() {
		driver.findElement(ConfigAdmin.Edit.saveNew).click();
		return this;
	}
	
	public UserManagerPage saveAndClose() {
		driver.findElement(ConfigAdmin.Edit.saveClose).click();
		return new UserManagerPage(driver);
	}
	
	public UserManagerPage close() {
		driver.findElement(ConfigAdmin.Edit.close).click();
		return new UserManagerPage(driver);
	}
	
	
	// ************************************************************************
	// AUXILIAR STRUCTURES -> TRYING A NEW APPROACH
	// ************************************************************************
	
	public static final class AccountDetails
	{
		public static final By name      = By.id("jform_name");
		public static final By loginName = By.id("jform_username");
		public static final By password  = By.id("jform_password");
		public static final By comfPass  = By.id("jform_password2");
		public static final By email     = By.id("jform_email");
		
		private AccountDetails() {}
	}

	public static final class AssignedUserGroups
	{
		public static final By publicGroup   = By.id("1group_1");
		public static final By registred     = By.id("1group_2");
		public static final By author        = By.id("1group_3");
		public static final By editor        = By.id("1group_4");
		public static final By publisher     = By.id("1group_5");
		public static final By manager       = By.id("1group_6");
		public static final By administrator = By.id("1group_7");
		public static final By superUser     = By.id("1group_8");
		public static final By guest         = By.id("1group_9");
		
		private AssignedUserGroups() {}
		
		// ************************************************************************
		// ONE BY ONE CHECK/UNCHECK GROUP
		// ************************************************************************
		
		public static void checkPublicGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.publicGroup);
			if (!group.isSelected()) {
				group.click();
			}
		}
		
		public static void uncheckPublicGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.publicGroup);
			if (group.isSelected()) {
				group.click();
			}
		}

		public static void checkRegisteredGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.registred);
			if (!group.isSelected()) {
				group.click();
			}
		}
		
		public static void uncheckRegisteredGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.registred);
			if (group.isSelected()) {
				group.click();
			}
		}

		public static void checkAuthorGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.author);
			if (!group.isSelected()) {
				group.click();
			}
		}
		
		public static void uncheckAuthorGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.author);
			if (group.isSelected()) {
				group.click();
			}
		}

		public static void checkEditorGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.editor);
			if (!group.isSelected()) {
				group.click();
			}
		}
		
		public static void uncheckEditorGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.editor);
			if (group.isSelected()) {
				group.click();
			}
		}

		public static void checkPublisherGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.publisher);
			if (!group.isSelected()) {
				group.click();
			}
		}
		
		public static void uncheckPublisherGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.publisher);
			if (group.isSelected()) {
				group.click();
			}
		}

		public static void checkManagerGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.manager);
			if (!group.isSelected()) {
				group.click();
			}
		}
		
		public static void uncheckManagerGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.manager);
			if (group.isSelected()) {
				group.click();
			}
		}
		
		public static void checkAdministratorGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.administrator);
			if (!group.isSelected()) {
				group.click();
			}
		}
		
		public static void uncheckAdministratorGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.administrator);
			if (group.isSelected()) {
				group.click();
			}
		}

		public static void checkSuperUserGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.superUser);
			if (!group.isSelected()) {
				group.click();
			}
		}
		
		public static void uncheckSuperUserGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.superUser);
			if (group.isSelected()) {
				group.click();
			}
		}

		public static void checkGuestGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.guest);
			if (!group.isSelected()) {
				group.click();
			}
		}
		
		public static void uncheckGuestGroup() {
			WebElement group = driver.findElement(AssignedUserGroups.guest);
			if (group.isSelected()) {
				group.click();
			}
		}
	}

}

