package com.joomla.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.config.ConfigAdmin;
import com.config.User;

/**
 * <h2>Joomla - Administrator Site</h2>
 * <p> Login Page
 * 
 * @author Tase Gula
 */
public class AdminLoginPage
{
	private final String url = ConfigAdmin.PageUrl.adminLogin;
	private final WebDriver driver;
	
	public static final By usernameLoc = By.id("mod-login-username");
	public static final By passwordLoc = By.id("mod-login-password");
	public static final By loginLoc    = By.xpath("//button[@class='btn btn-primary btn-large']");
	
	
	public AdminLoginPage(WebDriver driver) {
		this.driver = driver;
		
		if (!url.equals(driver.getCurrentUrl())) {
			System.err.println(this.getClass().getName() + " initialized on: " + driver.getCurrentUrl());
		}
	}
	
	public ControlPanelPage login(User user) {
		driver.findElement(usernameLoc).sendKeys(user.username());
		driver.findElement(passwordLoc).sendKeys(user.password());
		driver.findElement(loginLoc).click();
		
		return new ControlPanelPage(driver);
	}
	
	
	public ControlPanelPage logout() {
		
		return new ControlPanelPage(driver);
	}
	
}
