package com.joomla.home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.config.ConfigFront;

/**
 * <h2>Joomla - Front Site</h2>
 * <p> Submit an Article Page
 * 
 * @author Tase Gula
 */
public class ArticleFormPage
{
	private static String url = ConfigFront.PageUrl.submitArticle;
	private static WebDriver driver;
	
	// links
	public static final By saveButtonLoc     = By.xpath("//div[@class='btn-toolbar']/div[1]/button");
	public static final By cancelButtonLoc   = By.xpath("//div[@class='btn-toolbar']/div[1]/button");
	public static final By versionsButtonLoc = By.xpath("//div[@class='btn-toolbar']/div[1]/button");
	
	
	public ArticleFormPage(WebDriver driver) {
		ArticleFormPage.driver = driver;
		
		// Check that we're on the right page.
		if (!url.equals(ArticleFormPage.driver.getCurrentUrl())) {
			System.err.println("This is not the joomla home page on localhost.");
		}
	}
	
	
	public ArticleFormPage save() {
		driver.findElement(saveButtonLoc).click();
		return this;
	}
	
	public ArticleFormPage cancel() {
		driver.findElement(cancelButtonLoc).click();
		return this;
	}
	
	public ArticleFormPage versions() {
		driver.findElement(versionsButtonLoc).click();
		return this;
	}
	
	// ************************************************************************
	// CONTENT TAB CLASS + METHODS
	// ************************************************************************
	
	public static final class ContentTab
	{
		public static final By titleLoc = By.id("jform_title");
		public static final By aliasLoc = By.id("jform_alias");
		
		public static final By htmlEditorLoc  = By.id("jform_articletext");
		public static final By frameEditorLoc = By.id("jform_articletext_ifr");
		
		public static final By articleButtonLoc   = By.xpath("//a[@class='btn modal-button' and @title='Article']");
		public static final By imageButtonLoc     = By.xpath("//a[@class='btn modal-button' and @title='Image']");
		public static final By pageBreakButtonLoc = By.xpath("//a[@class='btn modal-button' and @title='Page Break']");
		public static final By toggleButtonloc    = By.xpath("//div[@class='toggle-editor btn-toolbar pull-right clearfix']/div/a");
		
		private ContentTab() {}
	}
	
	public ArticleFormPage typeTitle(String content) {
		driver.findElement(ContentTab.titleLoc).sendKeys(content);
		return this;
	}
	
	public ArticleFormPage typeHtmlText(String content) {
		WebElement toggleEditor = driver.findElement(ContentTab.toggleButtonloc);
		WebElement simpleEditor = driver.findElement(ContentTab.htmlEditorLoc);
		
		toggleEditor.click();
		simpleEditor.sendKeys(content);
		toggleEditor.click();
		return this;
	}
	
	// because this project has a didactic purpose, this method is using the iframe editor
	public ArticleFormPage typeFrameText(String content) {
		driver.switchTo().frame(driver.findElement(ContentTab.frameEditorLoc));
		driver.findElement(By.xpath("//body")).sendKeys(content + "\n");
		driver.switchTo().defaultContent();
		return this;
	}
	
	public ArticleFormPage typeAlias(String content) {
		driver.findElement(ContentTab.aliasLoc).sendKeys(content);
		return this;
	}
	
	
	public ArticleFormPage clear() {
		driver.findElement(ContentTab.titleLoc).clear();
		driver.findElement(ContentTab.aliasLoc).clear();
		
		clearBody();
		return this;
	}
	
	public ArticleFormPage clearBody() {
		WebElement toggleEditor = driver.findElement(ContentTab.toggleButtonloc);
		WebElement simpleEditor = driver.findElement(ContentTab.htmlEditorLoc);
		
		toggleEditor.click();
		simpleEditor.clear();
		toggleEditor.click();
		
		return this;
	}
	
	
	// ************************************************************************
	// PUBLISHING TAB CLASS + METHODS
	// ************************************************************************
	
	public static final class PublishingTab
	{
		By categoryLoc = By.id("jform_catid_chzn");
		By tagsLoc     = By.id("jform_tags_chzn");
		By versionLoc  = By.id("jform_version_note");
		By authorLoc   = By.id("jform_created_by_alias");
		By statusLoc   = By.id("jform_state_chzn");
		By featuredLoc = By.id("jform_featured_chzn");
		By accessLoc   = By.id("jform_access_chzn");
	}
	
	
	// ************************************************************************
	// LANGUAGE TAB CLASS + METHODS
	// ************************************************************************
	
	public static final class LanguageTab
	{
		By languageLoc = By.id("jform_language_chzn");
	}
	
	
	// ************************************************************************
	// METADATA TAB CLASS + METHODS
	// ************************************************************************
	
	public static final class MetadataTab
	{
		By metaLoc     = By.id("jform_metadesc");
		By keywordsLoc = By.id("jform_metakey");
	}
	
}
