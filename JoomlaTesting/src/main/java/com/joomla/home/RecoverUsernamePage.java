package com.joomla.home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.config.ConfigFront;

/**
 * <h2>Joomla - Front Site</h2> 
 * <p> Recover Username Page
 * 
 * @author Tase Gula
 */
public class RecoverUsernamePage
{
	private final String url =  ConfigFront.PageUrl.forgotUsername;
	private final WebDriver driver;
	private final WebDriverWait waitDriver;

	//links
	public static final By submitLocator  = By.xpath("//*[@id='user-registration']/div/button");	
	public static final By validateSubmit = By.xpath("//form[@class='form-validate']/div/button");
	
	public RecoverUsernamePage(WebDriver driver) {
		this.driver     = driver;
		this.waitDriver = new WebDriverWait(driver, 10);
		
		// Check that we're on the right page.
		if (!url.equals(this.driver.getCurrentUrl())) {
			System.err.println("This is not the create account page.");
		}
	}
	
	public RecoverUsernamePage recoverFor(String content) {
		waitDriver.until(ExpectedConditions.presenceOfElementLocated(submitLocator));
		driver.findElement(ConfigFront.Form.simpleMail).sendKeys(content);
		driver.findElement(submitLocator).click();
		return this;
	}
	
	public RecoverUsernamePage validateUsername(String content) {
		waitDriver.until(ExpectedConditions.presenceOfElementLocated(validateSubmit));
		driver.findElement(ConfigFront.Form.username).sendKeys(content);
		driver.findElement(validateSubmit).click();
		
		return this;
	}
	
	public RecoverUsernamePage typeNewPassword(String content) {
		waitDriver.until(ExpectedConditions.presenceOfElementLocated(validateSubmit));
		driver.findElement(ConfigFront.Form.password).sendKeys(content);
		driver.findElement(ConfigFront.Form.comfPass).sendKeys(content);
		driver.findElement(validateSubmit).click();
		
		return this;
	}
}
