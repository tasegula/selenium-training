package com.joomla.home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.config.ConfigFront;
import com.config.User;

/**
 * <h2>Joomla - Front Site</h2> 
 * <p> Create Account Page
 * 
 * @author Tase Gula
 */
public class CreateAccountPage
{
	private static final String url = ConfigFront.PageUrl.createAccount;
	private final WebDriver driver;

	// links
	public static final By regButton = By.xpath("//div[@class='form-actions']/button");
	public static final By regCancel = By.xpath("//div[@class='form-actions']/a");
	
	public CreateAccountPage(WebDriver driver) {
		this.driver     = driver;
		
		// Check that we're on the right page.
		if (!url.equals(this.driver.getCurrentUrl())) {
			System.err.println("This is not the create account page.");
		}
	}
	
	/**
	 * Fill the fields in the 'Create Account form' with the given user data
	 * 
	 * @param user
	 * @return
	 */
	public CreateAccountPage createFor(User user) {		
		driver.findElement(ConfigFront.Form.name).sendKeys(user.name());
		driver.findElement(ConfigFront.Form.username).sendKeys(user.username());
		driver.findElement(ConfigFront.Form.password).sendKeys(user.password());
		driver.findElement(ConfigFront.Form.comfPass).sendKeys(user.comfPass());
		driver.findElement(ConfigFront.Form.email).sendKeys(user.mailUser());
		driver.findElement(ConfigFront.Form.comfMail).sendKeys(user.comfMail());
		driver.findElement(regButton).click();
		return this;
	}
	
	// ************************************************************************
	// One By One Stuff
	// ************************************************************************
	
	public CreateAccountPage typeName(String content) {	
		driver.findElement(ConfigFront.Form.name).sendKeys(content);
		return this;
	}
	
	public CreateAccountPage typeUsername(String content) {
		driver.findElement(ConfigFront.Form.username).sendKeys(content);
		return this;
	}
	
	public CreateAccountPage typePassword(String content) {
		driver.findElement(ConfigFront.Form.password).sendKeys(content);
		return this;
	}
	
	public CreateAccountPage typeComfPass(String content) {
		driver.findElement(ConfigFront.Form.comfPass).sendKeys(content);
		return this;
	}
	
	public CreateAccountPage typeEmail(String content) {
		driver.findElement(ConfigFront.Form.email).sendKeys(content);
		return this;
	}
	
	public CreateAccountPage typeComfEmail(String content) {
		driver.findElement(ConfigFront.Form.comfMail).sendKeys(content);
		return this;
	}
	
	public CreateAccountPage clickRegister() {
		driver.findElement(regButton).click();
		return this;
	}
	
	public CreateAccountPage clickCancel() {
		driver.findElement(regCancel).click();
		return this;
	}
}
