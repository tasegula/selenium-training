package com.joomla.home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.config.ConfigFront;

/**
 * <h2>Joomla - Front Site</h2>
 * <p> Login Page
 * 
 * @author Tase Gula
 */
public class LoginPage
{
	private final WebDriver driver;
	private final WebDriverWait waitDriver;
	
	private final By loginLoc = By.xpath("//fieldset/div[@class='controls']/button");
	
	public LoginPage(WebDriver driver) {
		this.driver     = driver;
		this.waitDriver = new WebDriverWait(driver, 10);
	}
	
	public LoginPage loginWith(String username, String password) {
		waitDriver.until(ExpectedConditions.elementToBeClickable(loginLoc));
		driver.findElement(ConfigFront.Form.simpleUser).sendKeys(username);
		driver.findElement(ConfigFront.Form.simplePass).sendKeys(password);
		driver.findElement(loginLoc).click();
		
		return this;
	}
}
