package com.joomla.home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.config.ConfigFront;
import com.config.User;

/**
 * <h2>Joomla - Front Site</h2> 
 * <p> Home Page
 * 
 * @author Tase Gula
 */
public class HomePage
{
	private final String url = ConfigFront.PageUrl.home;
	private final WebDriver driver;
	private final WebDriverWait waitDriver;
	
	// links
	public static final By latestArticles    = By.xpath("//ul[@class='latestnews']/li");
	public static final By latestArticleLink = By.xpath(".//a");
	public static final By latestArticle     = By.xpath("//ul[@class='latestnews']/li[1]/a");
	
	public HomePage(WebDriver driver) {
		this.driver     = driver;
		this.waitDriver = new WebDriverWait(driver, 10);
		
		// Check that we're on the right page.
		if (!url.equals(this.driver.getCurrentUrl())) {
			System.err.println("This is not the joomla home page on localhost.");
		}
	}
	
	
	public HomePage login(User user) {
		driver.findElement(ConfigFront.MenuLogin.usernameLoc).sendKeys(user.username());
		driver.findElement(ConfigFront.MenuLogin.passwordLoc).sendKeys(user.password());
		driver.findElement(ConfigFront.MenuLogin.loginButtonLoc).click();
		
		return this;
	}
	
	public HomePage logout() {
		driver.findElement(ConfigFront.MenuLogin.logoutButtonLoc).click();
		return this;
	}
	
	public CreateAccountPage gotoCreateAccount() {		
		this.driver.findElement(ConfigFront.MenuLogin.createAccountLoc).click();

		waitDriver.until(ExpectedConditions.presenceOfElementLocated(ConfigFront.MenuLogin.createAccountLoc));
		return new CreateAccountPage(driver);
	}
	
	public RecoverUsernamePage gotoForgotUsername() {
		this.driver.findElement(ConfigFront.MenuLogin.forgotUsernameLoc).click();
		
		waitDriver.until(ExpectedConditions.presenceOfElementLocated(ConfigFront.MenuLogin.forgotUsernameLoc));
		return new RecoverUsernamePage(driver);
	}
	
	public RecoverPasswordPage gotoForgotPassword() {
		this.driver.findElement(ConfigFront.MenuLogin.forgotPasswordLoc).click();
		
		waitDriver.until(ExpectedConditions.presenceOfElementLocated(ConfigFront.MenuLogin.forgotPasswordLoc));
		return new RecoverPasswordPage(driver);
	}
	
	public ArticleFormPage gotoSubmitArticle() {
		this.driver.findElement(ConfigFront.MenuUser.submitArticleLoc).click();
		return new ArticleFormPage(driver);
	}
	
	public ArticlePage gotoLatestArticle() {
		driver.findElement(latestArticle).click();
		return new ArticlePage(driver);
	}
	
	public String getLatestArticleTitle() {
		return driver.findElement(latestArticle).getText();
	}
}
