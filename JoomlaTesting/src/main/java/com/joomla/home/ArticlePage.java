package com.joomla.home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * <h2>Testing Joomla</h2>
 * <p> An Article Page
 * 
 * @author Tase Gula
 */
public class ArticlePage
{
	private final WebDriver driver;
	
	// links
	public static final By articleTitle    = By.xpath("//div[@class='page-header']");
	public static final By articleBody     = By.xpath("//div[@itemprop='articleBody']");
	public static final By articleSettings = By.xpath("//div[@class='btn-group pull-right']");
	
	
	public ArticlePage(WebDriver driver) {
		this.driver     = driver;
	}
	
	
	public String getTitle() {
		return driver.findElement(articleTitle).getText();
	}
	
	public boolean canEdit() {
		return driver.findElements(articleSettings).size() > 0;
	}
	
	public ArticleFormPage gotoEdit() {
		WebElement settings = driver.findElement(articleSettings);
		settings.findElement(By.xpath(".//a")).click();
		settings.findElement(By.xpath(".//ul/li/a")).click();
		
		return new ArticleFormPage(driver);
	}
}
