package com.demostore.pages;

import java.util.ArrayList;
import java.util.List;

import net.thucydides.core.annotations.findby.FindBy;
import net.thucydides.core.pages.PageObject;

import org.openqa.selenium.WebElement;

public class CartPage extends PageObject {
	
	@FindBy(xpath = ".//a[@class='ty-cart-content__product-title']")
	private List<WebElement> productsList;
	
	/**
	 * returns a list with the names of the products in cart
	 */
	public List<String> getProductList() {
		List<String> products = new ArrayList<String>();
		for (WebElement we : productsList)
			products.add(we.getText());
		return products;
	}
	
	/**
	 * given the product name, returns the quantity info
	 */
	public String getProductQuantity(String productName) {
		return findBy("//div[@id='cart_items']//tr[.//a[.='" + productName + "']]//input[contains(@name,'amount')]").getAttribute("value");
	}
	
}
