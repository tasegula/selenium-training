package com.demostore.pages;

import net.thucydides.core.annotations.findby.FindBy;
import net.thucydides.core.pages.PageObject;
import net.thucydides.core.pages.WebElementFacade;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.demostore.util.Util;

public class ProductPage extends PageObject {
	
	@FindBy(xpath = ".//a[.//span[@class='ty-minicart-title ty-hand']]")
	private WebElementFacade cartView;
	
	@FindBy(xpath = ".//a[@class='ty-account-info__title']")
	private WebElementFacade myAccount;
	
	@FindBy(xpath = ".//button[contains(@id, 'button_cart')]")
	private WebElementFacade addToCartBtn;
	
	// ************************************************************************
	// LOCATORS
	// ************************************************************************
	
	private By viewCartLoc = By.xpath(".//a[@class='ty-btn ty-btn__secondary']");
	
	// ************************************************************************
	// API
	// ************************************************************************
	
	/**
	 * adds the current product to cart
	 */
	public void addProductToCart() {
		addToCartBtn.submit();
		Util.sleep(7500); // waitFor(ExpectedConditions.invisibilityofElementLocated()) doesn't work;
	}
	
	/**
	 * click on the shopping cart link
	 */
	public void navigateToCart() {
		Actions actions = new Actions(getDriver());
		actions.moveToElement(myAccount);
		actions.perform();
		
		cartView.click();
		WebElement link = getDriver().findElement(viewCartLoc);
		link.click();
	}
	
	// ************************************************************************
	// HELPERS
	// ************************************************************************
	
}
