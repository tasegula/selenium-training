package com.demostore.pages;

import java.util.List;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.findby.FindBy;
import net.thucydides.core.pages.PageObject;
import net.thucydides.core.pages.WebElementFacade;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.demostore.util.Util;

@DefaultUrl("http://demo.cs-cart.com/")
public class HomePage extends PageObject {
	
	@FindBy(xpath = ".//a[.//span[@class='ty-minicart-title ty-hand']]")
	private WebElementFacade cartView;
	
	@FindBy(xpath = ".//a[@class='ty-account-info__title']")
	private WebElementFacade myAccount;
	
	@FindBy(xpath = ".//div[@id='product_quick_view']//form[contains(@name, 'product_form')]")
	private WebElementFacade quickViewForm;
	
	@FindBy(xpath = ".//form[.//div[@class='ty-grid-list__item-name']]")
	private List<WebElement> quickViewProducts;
	
	// ************************************************************************
	// LOCATORS
	// ************************************************************************
	
	private By productTitleLoc = By.xpath(".//a[@class='product-title']");
	private By quickViewLoc = By.xpath(".//div[@class='ty-quick-view-button']/a");
	
	private By addToCartLoc = By.xpath(".//button[contains(@id, 'button_cart')]");
	private By viewCartLoc = By.xpath(".//a[@class='ty-btn ty-btn__secondary']");
	
	// ************************************************************************
	// API
	// ************************************************************************
	
	/**
	 * given the product, open the Quick View Form
	 */
	public void openQuickViewFor(String productName) {
		WebElement product = findProductInList(quickViewProducts, productName);
		WebElement quickLink = product.findElement(quickViewLoc);
		
		Actions actions = new Actions(getDriver());
		
		actions.moveToElement(product);
		actions.moveToElement(quickLink);
		actions.click();
		actions.perform();
	}
	
	/**
	 * adds the current <Quick View> product
	 */
	public void addQuickViewProductToCart() {
		quickViewForm.findBy(addToCartLoc).click();
		Util.sleep(7500); // waitFor(ExpectedConditions.invisibilityofElementLocated()) doesn't work;
	}
	
	/**
	 * given the product, open the Product Page
	 */
	public void openProductPageFor(String productName) {
		String link = ".//a[@class='product-title' and text()='" + productName + "']";
		By formLoc = By.xpath("//div[@class='ty-scroller-list__item' and " + link + "]");
		WebElementFacade form = find(formLoc);
		
		Actions actions = new Actions(getDriver());
		actions.moveToElement(form).perform();
		
		Util.highlight(getDriver(), form);
		
		form.findElement(By.xpath(link)).click();
	}
	
	/**
	 * click on the shopping cart link
	 */
	public void navigateToCart() {
		
		Actions actions = new Actions(getDriver());
		actions.moveToElement(myAccount);
		actions.perform();
		
		cartView.click();
		WebElement link = getDriver().findElement(viewCartLoc);
		link.click();
	}
	
	// ************************************************************************
	// HELPERS
	// ************************************************************************
	
	private WebElement findProductInList(List<WebElement> productList, String productName) {
		for (WebElement product : productList) {
			String candidate = product.findElement(productTitleLoc).getText();
			if (productName.equals(candidate)) {
				return product;
			}
		}
		
		return null;
	}
	
}
