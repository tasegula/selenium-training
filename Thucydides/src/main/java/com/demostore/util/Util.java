package com.demostore.util;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Util {
	
	public static final void sleep(long time) {
		try {
			Thread.sleep(time);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static final void sleep(long time, Object message) {
		print(message);
		sleep(time);
	}
	
	public static final void print(Object message) {
		String header = "+++++++++++++++++++++++++++++++++++++++++++++++++";
		
		System.out.println(header);
		System.out.println(message);
		System.out.println(header);
	}
	
	public static void highlight(WebDriver driver, WebElement element) {
		// draw a border around the found element
		String script = "arguments[0].style.border='3px solid red'";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(script, element);
	}
	
	public static final class ProductType {
		
		public static final int QUICK = 0;
		public static final int FULL = 1;
	}
}
