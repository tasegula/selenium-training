package com.demostore.test;

import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.runners.ThucydidesRunner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.demostore.requirements.OpenCart;
import com.demostore.steps.UserSteps;
import com.demostore.util.Util.ProductType;

@Story(OpenCart.Cart.AddProductToCart.class)
@RunWith(ThucydidesRunner.class)
public class AddProductTest {
	
	@Managed(uniqueSession = true)
	private WebDriver webDriver;
	
	@ManagedPages(defaultUrl = "http://demo.cs-cart.com/")
	private Pages pages;
	
	@Steps
	private UserSteps user;
	
	private String productQuick = "Apple iPhone 4S Black";
	private String productFull = "Packard Bell OneTwo";
	
	@Test
	public void addOneProductFromQuickView() {
		String productName = productQuick;
		int quantity = 1;
		
		user._open_home_page();
		user._quick_view_product(productName);
		user._add_product_to_cart(ProductType.QUICK);
		user._view_cart(ProductType.QUICK);
		user._check_product_in_cart(productName);
		user._check_quantity_for_product(productName, quantity);
	}
	
	@Test
	public void addOneProductFromProductPage() {
		String productName = productFull;
		int quantity = 1;
		
		user._open_home_page();
		user._view_product(productName);
		user._add_product_to_cart(ProductType.FULL);
		user._view_cart(ProductType.FULL);
		user._check_product_in_cart(productName);
		user._check_quantity_for_product(productName, quantity);
	}
}