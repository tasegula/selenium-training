package com.demostore.steps;

import java.util.List;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import org.junit.Assert;

import com.demostore.pages.CartPage;
import com.demostore.pages.HomePage;
import com.demostore.pages.ProductPage;
import com.demostore.util.Util.ProductType;

@SuppressWarnings("serial")
public class UserSteps extends ScenarioSteps {
	
	HomePage homePage;
	ProductPage productPage;
	CartPage cartPage;
	
	@Step
	public void _open_home_page() {
		homePage.open();
	}
	
	@Step
	public void _quick_view_product(String productName) {
		homePage.openQuickViewFor(productName);
	}
	
	@Step
	public void _view_product(String productName) {
		homePage.openProductPageFor(productName);
	}
	
	@Step
	public void _add_product_to_cart(int type) {
		if (type == ProductType.QUICK) {
			homePage.addQuickViewProductToCart();
		}
		else if (type == ProductType.FULL) {
			productPage.addProductToCart();
		}
	}
	
	@Step
	public void _view_cart(int type) {
		if (type == ProductType.QUICK) {
			homePage.navigateToCart();
		}
		else if (type == ProductType.FULL) {
			productPage.navigateToCart();
		}
	}
	
	@Step
	public void _check_product_in_cart(String productName) {
		List<String> cartProducts = cartPage.getProductList();
		Assert.assertTrue("Product not it cart",
						  cartProducts.contains(productName));
	}
	
	@Step
	public void _check_quantity_for_product(String productName, int quantity) {
		String actualQuantity = cartPage.getProductQuantity(productName);
		Assert.assertEquals("Incorrect quantity",
							"" + quantity, actualQuantity);
	}
}
