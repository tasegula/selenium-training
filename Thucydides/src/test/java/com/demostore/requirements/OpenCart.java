package com.demostore.requirements;

import net.thucydides.core.annotations.Feature;


public class OpenCart {

	@Feature
	public class Cart{
		public class AddProductToCart{}
		public class RemoveProductFromCart{}
	}
}
